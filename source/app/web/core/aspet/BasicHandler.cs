﻿using System.Web;
using app.web.core.stubs;

namespace app.web.core.aspet
{
  public class BasicHandler : IHttpHandler
  {
    ICreateRequests request_factory;
    IProcessRequests front_controller;

    public BasicHandler(ICreateRequests request_factory, IProcessRequests front_controller)
    {
      this.request_factory = request_factory;
      this.front_controller = front_controller;
    }

    public BasicHandler():this(new StubRequestFactory(),
      new FrontController())
    {
    }

    public void ProcessRequest(HttpContext context)
    {
      front_controller.process(request_factory.create_request_from(context));
    }

    public bool IsReusable
    {
      get { return true; }
    }
  }
}