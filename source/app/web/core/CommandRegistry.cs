﻿using System;
using System.Collections.Generic;
using System.Linq;
using app.web.core.stubs;

namespace app.web.core
{
  public class CommandRegistry : IFindCommands
  {
    IEnumerable<IProcessOneRequest> all_the_commands;
    CreateTheMissingCommand_Behaviour create_the_special_case;

    public CommandRegistry(IEnumerable<IProcessOneRequest> all_the_commands, CreateTheMissingCommand_Behaviour create_the_special_case)
    {
      this.all_the_commands = all_the_commands;
      this.create_the_special_case = create_the_special_case;
    }

    public CommandRegistry():this(new StubSetOfCommands(),() =>
    {
      throw new NotImplementedException();
    })
    {
    }

    public IProcessOneRequest get_the_command_that_can_handle(IContainRequestInformation request)
    {
      return all_the_commands.FirstOrDefault(x => x.can_process(request)) ?? create_the_special_case();
    }
  }
}