﻿using System;
using System.Data;
using System.Data.SqlClient;
using app.refactor_this.Rules;

namespace app.refactor_this
{
	public delegate Requisition GetCurrentRequisition_Behaviour();

	public delegate object GetLocalizedValueByKey_Behaviour(string key);

	public delegate int AddApplicationMessage(string message, string dialog_type, string error_type);

	public class LocalizationKeys
	{
		public const string bookperson_entertodate = "BookPerson-EnterToDate";
		public const string bookperson_fromdateisgreater = "BookPerson-FromDateIsGreater";
		public const string bookperson_distantfuture = "BookPerson-DistantFuture";
		public const string bookperson_datedifference = "BookPerson-DateDifference";
		public const string bookperson_entertotime = "BookPerson-EnterToTime";
		public const string bookperson_timeissame = "BookPerson-TimeIsSame";
		public const string bookperson_totimeislesser = "BookPerson-ToTimeIsLesser";
	}

	public class DateTimeRuleParameter
	{
		public bool Result { get; set; }
		public bool StopValidating { get; set; }
		public DateTime? CDateFrom { get; set; }
		public DateTime? CDateTo { get; set; }
		public string TimeFrom { get; set; }
		public string TimeTo { get; set; }
	}

	public class MessyCode
	{
		public string TimeFrom = null;
		public string TimeTo = null;
		public static GetCurrentRequisition_Behaviour current_requisition = () => BookPerson.requisition;
		public static GetLocalizedValueByKey_Behaviour localization_resolution = LocalizationManager.GetValue;
		public static AddApplicationMessage message_addition = ApplicationMessages.AddMessage;

		public bool IsDateTimeValid()
		{
			var current = current_requisition();

			var dateTimeRuleParameter = new DateTimeRuleParameter()
			{
				CDateFrom = current.DateFrom,
				CDateTo = current.DateTo,
				StopValidating = false,
				TimeFrom = TimeFrom,
				TimeTo = TimeTo,
			};

			IRuleChain ruleChain = new RuleChain(new DateFromEmptyRule());
			ruleChain.AddRuleToChain(new ToDateEmptyRule());
			ruleChain.AddRuleToChain(new FromDateLessThanToDate());
			ruleChain.AddRuleToChain(new ToDateTooFarInFuture());
			ruleChain.AddRuleToChain(new DateToTooFarFromDateFromRule());
			ruleChain.AddRuleToChain(new TimeFromIsEmptyRule());
			ruleChain.AddRuleToChain(new TimeToIsEmptyRule()); // no tests for this
			ruleChain.AddRuleToChain(new DateToAndDateFromAreNotEqualRule()); // no tests for this
			ruleChain.AddRuleToChain(new TimesAreAllEmptyRule()); // no tests for this
			ruleChain.AddRuleToChain(new FromTimeAndToTimeCannotBeTheSameRule()); // no tests for this
			ruleChain.AddRuleToChain(new ToTimeCannotBeLessThanFromTimeRule()); // no tests for this
			return ruleChain.RunRules(dateTimeRuleParameter, localization_resolution, message_addition);
		}
	}

	public class MessageType
	{
		public const string log_message = "log_message";
		public const string db_message = "db_message";
		public const string error = "error";
		public const string popup = "PopUp";
	}

	public class LocalizationManager
	{
		public static object GetValue(string bookperson_entertodate)
		{
			throw new NotImplementedException();
		}
	}

	public class ApplicationMessages
	{
		public static int AddMessage(string message, string message_type, string error)
		{
			if (message_type == "popup")
			{
				MessageDialog.show_error_popup(message, error);
				return 1;
			}
			if (message_type == "log_message")
			{
				LogManager.log_error(message, error);
				return 2;
			}
			if (message_type == "db_message")
			{
				DBLogger.log_error(message, error);
				return 3;
			}
			return 0;
			;
		}
	}

	public class DBLogger
	{
		public static void log_error(string message, string error)
		{
			var connection = new SqlConnection("data source=(local);Integrated Security=SSPI;Initial Catalog=errors");
			connection.Open();
			var command = connection.CreateCommand();
			command.CommandText = "InsertErrorMessage";
			command.CommandType = CommandType.StoredProcedure;

			var message_parameter = command.CreateParameter();
			message_parameter.ParameterName = "@p_message";
			message_parameter.Value = message;

			var error_parameter = command.CreateParameter();
			error_parameter.ParameterName = "@p_error";
			error_parameter.Value = error;

			command.Parameters.Add(message_parameter);
			command.Parameters.Add(error_parameter);

			try
			{
				command.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				LogManager.log_error("Logging failure", "Failure to log error message");
			}
			finally
			{
				connection.Close();
			}
		}
	}

	public class LogManager
	{
		public static void log_error(string message, string error)
		{
			throw new NotImplementedException();
		}
	}

	public class MessageDialog
	{
		public static void show_error_popup(string message, string error)
		{
			throw new NotImplementedException();
		}
	}

	public class RequisitionDAL
	{
		public static Requisition get_currently_active_requisition()
		{
			var connection = new SqlConnection("data source=(local);Integrated Security=SSPI;Initial Catalog=errors");
			connection.Open();
			var command = connection.CreateCommand();
			command.CommandText = "SELECT * FROM Requisitions";
			command.CommandType = CommandType.Text;

			try
			{
				var reader = command.ExecuteReader();
				var table = new DataTable();
				table.Load(reader);

				var requisition = new Requisition();
				var row = table.Rows[0];

				if (row["DateFrom"] != null) requisition.DateFrom = Convert.ToDateTime(row["DateFrom"]);
				if (row["DateTo"] != null) requisition.DateFrom = Convert.ToDateTime(row["DateTo"]);

				return requisition;
			}
			catch (Exception ex)
			{
				LogManager.log_error("Requisition Fetching", "Failed to get currently active requisition");
				return null;
			}
			finally
			{
				connection.Close();
			}
		}
	}

	public class Today
	{
		public static int Year
		{
			get { return DateTime.Now.Year; }
		}
	}

	public class BookPerson
	{
		static Requisition active_requisition;

		public static Requisition requisition
		{
			get { return active_requisition ?? (active_requisition = RequisitionDAL.get_currently_active_requisition()); }
		}
	}

	public class Requisition
	{
		public DateTime? DateTo { get; set; }
		public DateTime? DateFrom { get; set; }
	}
}