﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace app.refactor_this
{
	public class TimeSplitter
	{
		private char[] delimiters;
		private string time;

		public TimeSplitter(char[] delimiters, string time)
		{
			this.delimiters = delimiters;
			this.time = time;
		}

		private string[] SplitTime()
		{
			return time.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
		}

		private string GetMinutes()
		{
			return SplitTime()[0];
		}

		private string GetSeconds()
		{
			return SplitTime()[1];
		}

		public int GetMinutesInt()
		{
			return Convert.ToInt32(GetMinutes());
		}

		public int GetSecondsInt()
		{
			return Convert.ToInt32(GetSeconds());
		}

	}
}
