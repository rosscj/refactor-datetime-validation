﻿
namespace app.refactor_this.Rules
{
	public class RuleChain : IRuleChain
	{
		public IDateTimeRule FirstRule { get; private set; }
		private IRuleChain secondRule;

		public RuleChain(IDateTimeRule first)
		{
			FirstRule = first;
			secondRule = new NullRuleChain();
		}

		public bool RunRules(DateTimeRuleParameter dateTimeRuleParameter, GetLocalizedValueByKey_Behaviour localizationResolution, AddApplicationMessage addApplicationMessage)
		{
			FirstRule.Validate(dateTimeRuleParameter, localizationResolution, addApplicationMessage);
			
			if (dateTimeRuleParameter.StopValidating)
				return dateTimeRuleParameter.Result;

			//if (secondRule != null)
			if (secondRule.GetType() != typeof(NullRuleChain))
				return secondRule.RunRules(dateTimeRuleParameter, localizationResolution, addApplicationMessage);

			return true; // default value to return (this is valid if no rules return false) -- (old default was "//ToTime greater than FromTime")
		}

		public void AddRuleToChain(IDateTimeRule nextRule)
		{
			if (secondRule.GetType() == typeof(NullRuleChain))
			{
				secondRule = new RuleChain(nextRule);
				return;
			}

			secondRule.AddRuleToChain(nextRule);
		}
	}
}
