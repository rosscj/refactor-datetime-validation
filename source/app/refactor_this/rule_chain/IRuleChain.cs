namespace app.refactor_this.Rules
{
	public interface IRuleChain
	{
		bool RunRules(DateTimeRuleParameter dateTimeRuleParameter, GetLocalizedValueByKey_Behaviour localizationResolution, AddApplicationMessage addApplicationMessage);
		// RunRules signature has to match IDateTimeRule validate... better way to do this?  Parameter object?
		void AddRuleToChain(IDateTimeRule nextRule);
		IDateTimeRule FirstRule { get; }
	}
}