namespace app.refactor_this
{
	public class TimesAreAllEmptyRule : IDateTimeRule
	{
		public void Validate(DateTimeRuleParameter dateTimeRuleParameter, GetLocalizedValueByKey_Behaviour localizationResolution, AddApplicationMessage addApplicationMessage)
		{
			var delimiters = new[] { ':' };
			var fromTimeSplitter = new TimeSplitter(delimiters, dateTimeRuleParameter.TimeFrom);
			var toTimeSplitter = new TimeSplitter(delimiters, dateTimeRuleParameter.TimeTo);

			if (toTimeSplitter.GetMinutesInt() == 0 && toTimeSplitter.GetSecondsInt() == 0 &&
					fromTimeSplitter.GetMinutesInt() == 0 && fromTimeSplitter.GetSecondsInt() == 0)
			{
				dateTimeRuleParameter.StopValidating = true;
				dateTimeRuleParameter.Result = true;
			}
		}

		public string GetValidationFailedLocalizationKey()
		{
			throw new System.NotImplementedException();
		}
	}
}