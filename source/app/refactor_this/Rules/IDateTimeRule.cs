﻿namespace app.refactor_this
{
	public interface IDateTimeRule
	{
		void Validate(DateTimeRuleParameter dateTimeRuleParameter, GetLocalizedValueByKey_Behaviour localizationResolution, AddApplicationMessage addApplicationMessage);
		string GetValidationFailedLocalizationKey();
	}
}
