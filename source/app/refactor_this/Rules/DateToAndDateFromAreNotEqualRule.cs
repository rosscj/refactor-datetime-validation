using System;

namespace app.refactor_this
{
	public class DateToAndDateFromAreNotEqualRule : IDateTimeRule
	{
		public void Validate(DateTimeRuleParameter dateTimeRuleParameter, GetLocalizedValueByKey_Behaviour localizationResolution, AddApplicationMessage addApplicationMessage)
		{
			if (DateTime.Compare(dateTimeRuleParameter.CDateFrom.Value, dateTimeRuleParameter.CDateTo.Value) != 0)
			{
				dateTimeRuleParameter.StopValidating = true;
				dateTimeRuleParameter.Result = true;
			}
		}

		public string GetValidationFailedLocalizationKey()
		{
			throw new System.NotImplementedException();
		}
	}
}