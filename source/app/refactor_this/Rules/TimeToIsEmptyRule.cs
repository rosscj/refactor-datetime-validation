namespace app.refactor_this
{
	public class TimeToIsEmptyRule : IDateTimeRule
	{
		public void Validate(DateTimeRuleParameter dateTimeRuleParameter, GetLocalizedValueByKey_Behaviour localizationResolution, AddApplicationMessage addApplicationMessage)
		{
			if (dateTimeRuleParameter.TimeTo == string.Empty)
			{
				addApplicationMessage(localizationResolution(GetValidationFailedLocalizationKey()).ToString(), MessageType.popup,
				                      MessageType.error);

				dateTimeRuleParameter.StopValidating = true;
				dateTimeRuleParameter.Result = false;
			}
		}

		public string GetValidationFailedLocalizationKey()
		{
			return LocalizationKeys.bookperson_entertotime;
		}
	}
}