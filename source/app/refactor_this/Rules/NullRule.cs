﻿namespace app.refactor_this.Rules
{
	public class NullRule : IDateTimeRule
	{
		public void Validate(DateTimeRuleParameter dateTimeRuleParameter, GetLocalizedValueByKey_Behaviour localizationResolution, AddApplicationMessage addApplicationMessage)
		{
			dateTimeRuleParameter.Result = false;
		}

		public string GetValidationFailedLocalizationKey()
		{
			throw new System.NotImplementedException();
		}
	}
}
