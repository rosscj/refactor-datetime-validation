namespace app.refactor_this
{
	public class DateFromEmptyRule : IDateTimeRule
	{
		public void Validate(DateTimeRuleParameter dateTimeRuleParameter, GetLocalizedValueByKey_Behaviour localizationResolution, AddApplicationMessage addApplicationMessage)
		{
			if (dateTimeRuleParameter.CDateFrom == null)
			{
				dateTimeRuleParameter.StopValidating = true;
				dateTimeRuleParameter.Result = NoAdditionalInformationProvided(dateTimeRuleParameter);
			}
		}

		public string GetValidationFailedLocalizationKey()
		{
			return "";
		}

		private static bool NoAdditionalInformationProvided(DateTimeRuleParameter dateTimeRuleParameter)
		{
			return dateTimeRuleParameter.CDateTo == null &&
			       string.IsNullOrEmpty(dateTimeRuleParameter.TimeFrom) &&
			       string.IsNullOrEmpty(dateTimeRuleParameter.TimeTo);
		}
	}
}