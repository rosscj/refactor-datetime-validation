namespace app.refactor_this
{
	public class DateToTooFarFromDateFromRule : IDateTimeRule
	{
		private int daysDifferenceThreshold = 100;

		public void Validate(DateTimeRuleParameter dateTimeRuleParameter, GetLocalizedValueByKey_Behaviour localizationResolution, AddApplicationMessage addApplicationMessage)
		{
			if (dateTimeRuleParameter.CDateTo.Value.Subtract(dateTimeRuleParameter.CDateFrom.Value).TotalDays > daysDifferenceThreshold)
			{
				addApplicationMessage(localizationResolution(GetValidationFailedLocalizationKey()).ToString(), MessageType.popup,
				                      MessageType.error);

				dateTimeRuleParameter.StopValidating = true;
				dateTimeRuleParameter.Result = false;
			}
		}

		public string GetValidationFailedLocalizationKey()
		{
			return LocalizationKeys.bookperson_datedifference;
		}
	}
}