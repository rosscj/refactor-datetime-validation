namespace app.refactor_this
{
	public class ToDateTooFarInFuture : IDateTimeRule
	{
		private const int _yearThreshold = 10;

		public void Validate(DateTimeRuleParameter dateTimeRuleParameter, GetLocalizedValueByKey_Behaviour localizationResolution, AddApplicationMessage addApplicationMessage)
		{
			if ((dateTimeRuleParameter.CDateTo.Value.Year - Today.Year) >= _yearThreshold)
			{
				addApplicationMessage(localizationResolution(GetValidationFailedLocalizationKey()).ToString(), MessageType.popup,
				                      MessageType.error);

				dateTimeRuleParameter.StopValidating = true;
				dateTimeRuleParameter.Result = false;
			}
		}

		public string GetValidationFailedLocalizationKey()
		{
			return LocalizationKeys.bookperson_distantfuture;
		}
	}
}