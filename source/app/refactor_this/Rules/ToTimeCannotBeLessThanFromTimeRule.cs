namespace app.refactor_this
{
	public class ToTimeCannotBeLessThanFromTimeRule : IDateTimeRule
	{
		public void Validate(DateTimeRuleParameter dateTimeRuleParameter, GetLocalizedValueByKey_Behaviour localizationResolution, AddApplicationMessage addApplicationMessage)
		{
			var delimiters = new[] { ':' };
			var fromTimeSplitter = new TimeSplitter(delimiters, dateTimeRuleParameter.TimeFrom);
			var toTimeSplitter = new TimeSplitter(delimiters, dateTimeRuleParameter.TimeTo);

			if ((toTimeSplitter.GetMinutesInt() < fromTimeSplitter.GetMinutesInt())
				||
				((toTimeSplitter.GetMinutesInt() == fromTimeSplitter.GetMinutesInt()) &&
				(toTimeSplitter.GetSecondsInt() < fromTimeSplitter.GetSecondsInt())))
			{
				addApplicationMessage(localizationResolution(GetValidationFailedLocalizationKey()).ToString(), MessageType.popup,
				                      MessageType.error);

				dateTimeRuleParameter.StopValidating = true;
				dateTimeRuleParameter.Result = false;
			}
		}

		public string GetValidationFailedLocalizationKey()
		{
			return LocalizationKeys.bookperson_totimeislesser;
		}
	}
}