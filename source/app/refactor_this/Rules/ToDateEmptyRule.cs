namespace app.refactor_this
{
	public class ToDateEmptyRule : IDateTimeRule
	{
		public void Validate(DateTimeRuleParameter dateTimeRuleParameter, GetLocalizedValueByKey_Behaviour localizationResolution, AddApplicationMessage addApplicationMessage)
		{
			if (dateTimeRuleParameter.CDateTo == null)
			{
				addApplicationMessage(localizationResolution(GetValidationFailedLocalizationKey()).ToString(), MessageType.popup,
				                      MessageType.error);

				dateTimeRuleParameter.StopValidating = true;
				dateTimeRuleParameter.Result = false;
			}
		}

		public string GetValidationFailedLocalizationKey()
		{
			return LocalizationKeys.bookperson_entertodate;
		}
	}
}