﻿using System;

namespace app.refactor_this
{
	public class FromDateLessThanToDate : IDateTimeRule
	{
		public void Validate(DateTimeRuleParameter dateTimeRuleParameter, GetLocalizedValueByKey_Behaviour localizationResolution, AddApplicationMessage addApplicationMessage)
		{
			if (DateTime.Compare(dateTimeRuleParameter.CDateFrom.Value, dateTimeRuleParameter.CDateTo.Value) > 0)
			{

				addApplicationMessage(localizationResolution(GetValidationFailedLocalizationKey()).ToString(), MessageType.popup,
				                      MessageType.error);

				dateTimeRuleParameter.StopValidating = true;
				dateTimeRuleParameter.Result = false;
			}

		}

		public string GetValidationFailedLocalizationKey()
		{
			return LocalizationKeys.bookperson_fromdateisgreater;
		}
	}
}
