namespace app.refactor_this
{
	public class TimeFromIsEmptyRule : IDateTimeRule
	{
		public void Validate(DateTimeRuleParameter dateTimeRuleParameter, GetLocalizedValueByKey_Behaviour localizationResolution, AddApplicationMessage addApplicationMessage)
		{
			if (dateTimeRuleParameter.TimeFrom == string.Empty)
			{
				dateTimeRuleParameter.StopValidating = true;
				dateTimeRuleParameter.Result = true;
			}
		}

		public string GetValidationFailedLocalizationKey()
		{
			return "";
		}
	}
}