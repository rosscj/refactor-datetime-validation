﻿using System;
using Machine.Specifications;
using app.refactor_this;
using developwithpassion.specifications.rhinomocks;

namespace app.specs
{
	[Subject(typeof(MessyCode))]
	public class MessyCodeSpecs
	{
		public abstract class concern : Observes<MessyCode>
		{
		}

		public class sanity_check : concern
		{
			private Because b = () =>
													result = true;

			private It should_run_test = () =>
																	 result.ShouldBeTrue();

			private static bool result;
		}

		public class when_determining_if_the_date_is_valid : concern
		{
			//private int x = 0;

			//public when_determining_if_the_date_is_valid()
			//{
			//  x = 100;
			//}

			Establish c = () =>
			{
				try
				{
					current_requisition = new Requisition();
					GetCurrentRequisition_Behaviour item = () => current_requisition;
					spec.change(() => MessyCode.current_requisition).to(item);
				}
				catch (Exception)
				{
					Console.WriteLine("cjr caught Establish exception");
					throw new Exception("cjr caught this");
				}
			};

			Because b = () =>
				result = sut.IsDateTimeValid();

			public class and_the_end_date_is_before_to_date
			{
				private Establish c = () =>
																{
																	current_requisition.DateFrom = DateTime.Now.AddDays(-1);
																	current_requisition.DateTo = DateTime.Now.AddDays(-4);

																	GetLocalizedValueByKey_Behaviour key_fetch = x =>
																	{
																		x.ShouldEqual(LocalizationKeys.bookperson_fromdateisgreater);
																		return "bookperson_fromdateisgreater";
																	};
																	AddApplicationMessage message_add = (message, type, error_type) =>
																	{
																		message.ShouldEqual("bookperson_fromdateisgreater");
																		type.ShouldEqual(MessageType.popup);
																		error_type.ShouldEqual(MessageType.error);
																		return 0;
																	};
																	spec.change(() => MessyCode.localization_resolution).to(key_fetch);
																	spec.change(() => MessyCode.message_addition).to(message_add);

																};

				Because b = () =>
						result = sut.IsDateTimeValid();
	

				private It should_be_invalid_with_bookperson_fromdateisgreater_msg = () =>
																			 result.ShouldBeFalse();
			}

			public class and_date_to_is_more_than_100_days_after_from_date
			{
				Establish c = () =>
				{
					current_requisition.DateFrom = DateTime.Now;
					current_requisition.DateTo = DateTime.Now.AddDays(101);

					GetLocalizedValueByKey_Behaviour key_fetch = x =>
					{
						x.ShouldEqual(LocalizationKeys.bookperson_datedifference);
						return "bookperson_datedifference";
					};
					AddApplicationMessage message_add = (message, type, error_type) =>
					{
						message.ShouldEqual("bookperson_datedifference");
						type.ShouldEqual(MessageType.popup);
						error_type.ShouldEqual(MessageType.error);
						return 0;
					};
					spec.change(() => MessyCode.localization_resolution).to(key_fetch);
					spec.change(() => MessyCode.message_addition).to(message_add);

				};

				It should_be_invalid_with_bookperson_datedifference_msg = () =>
											 result.ShouldBeFalse();
			}

			public class and_date_to_is_less_than_100_days_after_from_date_and_from_time_is_empty
			{
				Establish c = () =>
				{
					current_requisition.DateFrom = DateTime.Now;
					current_requisition.DateTo = DateTime.Now.AddDays(99);

					// this is not NEEDED since call succeeds, but will leave it i guess...
					GetLocalizedValueByKey_Behaviour key_fetch = x =>
					{
						x.ShouldEqual(LocalizationKeys.bookperson_datedifference);
						return "bookperson_datedifference";
					};
					AddApplicationMessage message_add = (message, type, error_type) =>
					{
						message.ShouldEqual("bookperson_datedifference");
						type.ShouldEqual(MessageType.popup);
						error_type.ShouldEqual(MessageType.error);
						return 0;
					};
					spec.change(() => MessyCode.localization_resolution).to(key_fetch);
					spec.change(() => MessyCode.message_addition).to(message_add);

				};

				It should_be_valid = () =>
											 result.ShouldBeTrue();
			}

			public class and_date_to_is_more_than_10_years_in_future
			{
				private Establish c = () =>
				{
					current_requisition.DateFrom = DateTime.Now;
					current_requisition.DateTo = DateTime.Now.AddYears(11);

					GetLocalizedValueByKey_Behaviour key_fetch = x =>
					{
						x.ShouldEqual(LocalizationKeys.bookperson_distantfuture);
						return "bookperson_distantfuture";
					};
					AddApplicationMessage message_add = (message, type, error_type) =>
					{
						message.ShouldEqual("bookperson_distantfuture");
						type.ShouldEqual(MessageType.popup);
						error_type.ShouldEqual(MessageType.error);
						return 0;
					};
					spec.change(() => MessyCode.localization_resolution).to(key_fetch);
					spec.change(() => MessyCode.message_addition).to(message_add);

				};

				Because b = () =>
					result = sut.IsDateTimeValid();

				private It should_be_invalid_with_bookperson_distantfuture_msg = () =>
															 result.ShouldBeFalse();

			}

			public class and_there_is_no_from_date
			{
				Establish c = () =>
				{
					current_requisition.DateFrom = null;
				};

				public class and_no_other_date_information_is_present
				{
					It should_be_valid = () =>
						result.ShouldBeTrue();
				}

				public class and_to_date_is_present
				{
					Establish c = () =>
					{
						current_requisition.DateTo = DateTime.Now.AddDays(2);
					};

					Because b = () =>
						result = sut.IsDateTimeValid();

					It should_be_invalid_with_no_msg = () =>
						result.ShouldBeFalse();

				}

			}

			public class and_there_is_a_from_date
			{
				Establish c = () =>
				{
					current_requisition.DateFrom = new DateTime(2011, 10, 10);
				};

				public class and_no_to_date_has_been_provided
				{
					Establish c = () =>
					{
						GetLocalizedValueByKey_Behaviour key_fetch = x =>
						{
							x.ShouldEqual(LocalizationKeys.bookperson_entertodate);
							return "blah";
						};
						AddApplicationMessage message_add = (message, type, error_type) =>
						{
							message.ShouldEqual("blah");
							type.ShouldEqual(MessageType.popup);
							error_type.ShouldEqual(MessageType.error);
							return 0;
						};
						spec.change(() => MessyCode.localization_resolution).to(key_fetch);
						spec.change(() => MessyCode.message_addition).to(message_add);
					};

					It should_be_invalid_with_bookperson_entertodate_msg = () =>
						result.ShouldBeFalse();

				}
			}

			static bool result;
			static Requisition current_requisition;
		}
	}
}